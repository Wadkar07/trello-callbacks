const boards = require("./boards.json")

function callback1(boardID, callback) {
    if ((boardID === undefined || typeof boardID !== 'string') || typeof callback !== 'function') {
        callback(new Error('error : Invalid board id.'));
    } else {
        setTimeout(() => {
            let boardDetail = boards.find(board => {
                if (board.id === boardID) {
                    return board;
                }
            });
            if (boardDetail) {
                callback(null, boardDetail);
            } else {
                callback(new Error('Board ID does not exist'));
            }
        }, 2 * 1000);
    }
}
module.exports = callback1;

