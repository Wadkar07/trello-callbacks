const callback1 = require('./callback1.cjs');
const callback2 = require('./callback2.cjs');
const callback3 = require('./callback3.cjs');

const boards = require('./boards.json');

function callback5(callback) {
    setTimeout(() => {
        const requiredBoard = 'Thanos';
        const thanos = boards.find((board) => {
            return board.name === requiredBoard;
        });
        console.log(thanos);
        const thanosId = thanos.id;
        callback1(thanosId, (err, data) => {
            if (err) {
                console.error(new Error(`Error finding ${thanosId} : ${err}`));
            }
            else {
                const listId = data.id;
                callback2(listId, (err, listData) => {
                    if (err) {
                        callback(`Error while finding ${listId} : ${err}`);
                    }
                    else {
                        console.log(listData);
                        let requiredListName = 'Mind';
                        let requiredList = listData[listId].find((boardList) => {
                            return boardList.name === requiredListName;
                        });
                        let requiredCardId = requiredList.id;
                        callback3(requiredCardId, (err, data) => {
                            if (err) {
                                callback(new Error (`Error while finding ${requiredCardId} : ${err}`));
                            }
                            else {
                                console.log(data);
                                requiredListName = 'Space';
                                requiredList = listData[listId].find((boardList) => {
                                    return boardList.name === requiredListName;
                                });
                                requiredCardId = requiredList.id;
                                callback3(requiredCardId, (err, data) => {
                                    if (err) {
                                        callback(new Error(`Error while finding ${requiredCardId} : ${err}`));
                                    }
                                    else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}
module.exports = callback5;