/* 
Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require('./cards.json')
function callback3(listID, callback) {
    if ((listID === undefined || typeof listID !== 'string') || typeof callback !== 'function') {
        callback(new Error('error : Invalid board id.'));
    } else {
        setTimeout(() => {
            let cardList = Object.entries(cards)
                .filter((card) => {
                    return card[0] === listID;
                });
            cardList = Object.fromEntries(cardList);
            if (cardList) {
                callback(null, cardList);
            } else {
                callback(new Error('Board ID does not exist'));
            }
        }, 2 * 1000);
    }
}
module.exports = callback3;
