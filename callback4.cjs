const callback1 = require('./callback1.cjs');
const callback2 = require('./callback2.cjs');
const callback3 = require('./callback3.cjs');

const boards = require('./boards.json');


function callback4(callback) {
    if(typeof callback == 'function'){
        setTimeout(() => {

            const requiredBoard = 'Thanos';
            const thanos = boards.find((board) => {
                return board.name === requiredBoard;
            });
            console.log(thanos);
            const thanosId = thanos.id;
            callback1(thanosId, (err, data) => {
                if (err) {
                    callback(new Error(`No such id ${thanosId} found ${err}`));
                }
                else {
                    const listId = data.id;
                    callback2(listId, (err, data) => {
                        if (err) {
                            callback(new Error (`Error while finding ${listId} : ${err}`));
                        }
                        else {
                            console.log(data);
                            const requiredListName = 'Mind';
                            const requiredList = data[listId].find((boardList) => {
                                return boardList.name === requiredListName;
                            });
                            const requiredCardId = requiredList.id;
                            callback3(requiredCardId, (err, data) => {
                                if (err) {
                                    callback(new Error (`Error while finding ${requiredCardId} : ${err}`));
                                }
                                else {
                                    console.log(data);
                                }
                            });
                        }
                    });
                }
            });
        }, 2 * 1000);
    }
    else{
        console.log('Invalid argument');
    }
}
module.exports = callback4;