
const callback1 = require('./callback1.cjs');
const callback2 = require('./callback2.cjs');
const callback3 = require('./callback3.cjs');

const boards = require('./boards.json');

function callback6(callback) {
    setTimeout(() => {
        const requiredBoard = 'Thanos';
        const thanos = boards.find((board) => {
            return board.name === requiredBoard;
        });
        console.log(thanos);
        const thanosId = thanos.id;
        callback1(thanosId, (err, thanosData) => {
            if (err) {
                callback(new Error(`Error finding ${thanosId} : ${err}`));
            }
            else {
                const listId = thanosData.id;
                callback2(listId, (err, listData) => {
                    if (err) {
                        callback(new Error(`Error finding ${listId} : ${err}`));
                    }
                    else {
                        const cardIds = listData[thanosId].map((currentList)=>{
                            return currentList.id;
                        });
                        function getCards(index){
                            if(index < cardIds.length){
                                let card = cardIds[index];
                                callback3(card,(err,cardData)=>{
                                    if(err){
                                        callback(new Error(`Error with ${cardData}:${err}`));
                                    }
                                    else{
                                        if(Object.keys(cardData).length>0){
                                            console.log(cardData);
                                        }
                                    }
                                });
                                index++;
                                getCards(index);
                            }
                        }                        
                        getCards(0);
                    }
                });
            }
        });
    }, 2 * 1000);
}
module.exports = callback6;