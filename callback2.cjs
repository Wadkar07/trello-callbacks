const lists = require('./lists.json')
function callback2(boardID, callback) {
    if ((boardID === undefined || typeof boardID !== 'string') || typeof callback !== 'function') {
        callback(new Error('error : Invalid board id.'));
    } else {
        setTimeout(() => {
            let boardList = Object.entries(lists)
                .filter((list) => {
                    return list[0] === boardID;
                });
            boardList = Object.fromEntries(boardList);
            if (boardList) {
                callback(null, boardList);
            } else {
                callback(new Error('Board ID does not exist'));
            }
        }, 2 * 1000);
    }
}
module.exports = callback2;